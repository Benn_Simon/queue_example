package queue.Sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import queue.Model.TransportRequest;

@Component
public class MessageSender {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private @Qualifier("queue") Queue queue;

    public void send(TransportRequest transportRequest){
        System.out.println("Sent "+transportRequest.toString());
        template.convertAndSend(queue.getName(), transportRequest);
    }
}
