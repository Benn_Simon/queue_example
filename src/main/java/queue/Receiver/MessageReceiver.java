package queue.Receiver;

import com.rabbitmq.client.Channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.ui.ModelMap;
import queue.Model.TransportRequest;
import queue.Sender.MessageSender;

import java.io.IOException;

@RabbitListener(queues = "Test!")
public class MessageReceiver {

    private static final Logger log = LoggerFactory.getLogger(MessageReceiver.class);

//    @RabbitHandler
//    public void receive(TransportRequest
//                                     request, Channel channel,
//                        @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
//
//        System.out.println("Received "+request.toString());
//        //messageSender.send(request);
//        //channel.basicAck(tag ,false);
//    }
}
