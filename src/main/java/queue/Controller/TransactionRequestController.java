package queue.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import queue.Library.GenerateFakeData;
import queue.Sender.MessageSender;

@RestController
@RequestMapping("/queue")
public class TransactionRequestController {
    @Autowired
    MessageSender messageSender;
    @Autowired
    GenerateFakeData generateFakeData;
    @RequestMapping("/")
    public ResponseEntity<ModelMap> sendRequestToQueue(){
        ModelMap modelMap = new ModelMap();
        String message =  "";
        messageSender.send(generateFakeData.generateFakeTransportRequest());
        modelMap.put("message","Item Sent to the queue");
        return ResponseEntity.ok(modelMap);
    }

}
