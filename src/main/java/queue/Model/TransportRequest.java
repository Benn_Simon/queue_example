package queue.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class TransportRequest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long userid;
    private String item;
    private String destination;
    private String origin;
    private double weight;//approximate
    private double price;
    private String createdon;

    public TransportRequest(long userid, String item, String destination, String origin, double weight, double price, String createdon) {
        this.userid = userid;
        this.item = item;
        this.destination = destination;
        this.origin = origin;
        this.weight = weight;
        this.price = price;
        this.createdon = createdon;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public long getId() {
        return id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString(){
        return this.getItem()+":"+this.getDestination()+":"+this.getOrigin()+":"+this.getWeight();
    }

    public static String[] names(){
        return new String[]{"item", "destination", "origin", "weight", "price"};
    }
}
