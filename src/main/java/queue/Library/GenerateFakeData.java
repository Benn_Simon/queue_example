package queue.Library;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.abstraction.MockUnit;
import net.andreinc.mockneat.types.enums.DictType;
import net.andreinc.mockneat.types.enums.NameType;
import org.springframework.stereotype.Component;
import queue.Model.TransportRequest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

@Component
public class GenerateFakeData {

    static MockNeat InitMock() {
        return MockNeat.threadLocal();
    }

    public static void main(String[] args){
        generateFakeTransportRequestCsv();
    }

    public TransportRequest generateFakeTransportRequest() {
        MockNeat m = InitMock();
        //long userid, String item, String destination, String origin, double weight, double price, String createdon
        MockUnit<TransportRequest> transportRequestGenerator =
                m.constructor(TransportRequest.class).params(
                        1,
                        m.dicts().type(DictType.EN_NOUN_4SYLL).noSpecialChars(),
                        m.cities().us(),
                        m.cities().us(),
                        m.doubles().range(1.0, 1000.0),
                        0.0,
                        m.localDates().toUtilDate().mapToString()
                );
        System.out.println(transportRequestGenerator.val().toString());
        return transportRequestGenerator.val();
    }

    public static void generateFakeTransportRequestCsv() {
        MockNeat m = InitMock();
        //long userid, String item, String destination, String origin, double weight, double price, String createdon
        String headers = "id,userid,item,destination,origin,weight,price,createdon";
        final Path path = Paths.get("./"+m.strings().val()+".csv");
        m.fmt("#{id},#{userid},#{item},#{destination},#{origin},#{weight},#{price},#{createdon}")
                .param("id",m.ints().range(0, 1))
                .param("userid", m.dicts().type(DictType.EN_NOUN_4SYLL).noSpecialChars())
                .param("item", m.names().last())
                .param("destination", m.cities().us())
                .param("origin", m.cities().us())
                .param("weight", m.doubles().range(1.0, 1000.0))
                .param("price", m.ints().range(0, 1))
                .param("createdon", m.localDates().toUtilDate().mapToString())
                .list(100)
                .consume(list -> {
                    list.add(0,headers);
                    list.forEach(s -> System.out.println(s));
                    try {
                        Files.write(path, list, CREATE, WRITE);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                });

    }

}
