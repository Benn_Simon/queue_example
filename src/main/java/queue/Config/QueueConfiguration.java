package queue.Config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import queue.Receiver.MessageReceiver;
import queue.Sender.MessageSender;

@Configuration
public class QueueConfiguration {

    @Bean
    public Queue queue(){
        return new Queue("Test!",true, false, false, null);
    }
    @Bean
    TopicExchange topicExchange(){
        return new TopicExchange("test-exchange");
    }

    @Bean
    Binding binding(@Qualifier("queue")Queue queue, TopicExchange topicExchange){
        return BindingBuilder.bind(queue).to(topicExchange).with(queue.getName());
    }


    @Bean
    public MessageReceiver receiver(){
        return new MessageReceiver();
    }

    @Bean
    public MessageSender sender(){
        return new MessageSender();
    }
}
